# BJO102

Welcome to the app that powers the **bjo102.dev** website!

This project is all about repositories - getting data from **GitLab APIs**, and passing it to **React components** that return archives and single pages. The idea is presenting the chosen projects dynamically, while maintaining *one source of truth* by using GitLab as the *server* of information.

The **client** directory holds files for the UI, and the **server** directory holds the app for the proxy server.

## Features

- Query for Remote repositories
- Archive for repositories
- Page template for repositories
- Proxy server
- Email form
- Markdown parsing & rendering
- Routing

## Technologies

- React
- GraphQL
- Express
- Nodemailer
- Traefik

## Installation & setup

The project has two parts, **client** and **server**, as seen by the respective folders. Each folder has the `package.json` files set up.

Make adjustments to the `name`, `version`, `description`, `homepage`, `proxy`, `bugs`, and `repository` properties of both `package.json` files.

There's no need to run `npm install` as this is done in the **docker containers**.

### The Environment

Add a new `.env` file to the root directory of this project. Then, add the following content.

    # DOMAINS & ROUTER_IDs for Traefik
    ## NOTE: the value for ROUTER*_ID should not contain " . "
    ## EXAMPLE: `ROUTER_ID=example.no` should be `ROUTER_ID=exampleno`

    # Client app vars
    CLIENT_APP_ROUTER_ID=domaincom
    CLIENT_APP_DOMAIN=domain.com
    ClIENT_APP_PORT=3000

    # Server app vars
    SERVER_APP_ROUTER_ID=apidomaincom
    SERVER_APP_DOMAIN=api.domain.com
    SERVER_APP_PORT=3001

    # Email details
    EMAIL=email-address
    EMAIL_PASS=email-password
    EMAIL_OAUTH_CLIENTID=client-id
    EMAIL_OAUTH_CLIENT_SECRET=client-secret
    EMAIL_OAUTH_REFRESH_TOKEN=refresh-token

    # Gitlab/Github tokens
    REACT_APP_GITLAB_AUTH_TOKEN=your-gitlab-token

Both client and server apps will use variables from this file, as they are passed into the containers by the docker-compose files

Domains and Router IDs are mainly for Traefik. However, the domain name and ports for the server app are also important for the client during the runtime without Traefik. It's best to leave everything in there, whatever build you're using.

If you want the contact form to work, you will have to get your email tokens set up. See links below for help.

### Choose your projects

In the `client/src/projects` folder is the `ProjectSpecs.js` module.

This is where you will provide IDs and other specifications for the GitLab projects you want to display on the app. At runtime, this module is imported to the app, and is used to pull from GitLab APIs. The response data is mapped to the appropriate data points within the ProjectSpecs module. At this point, all the information is committed to React state and context for the whole application.

Go and see the file for more details.

#### README.md

When the app runs the loop for the `ProjectSpecs.js` module, the GitLab GraphQL API is used to pull raw content from the **README.md** for each project. The app checks to see if **README.md** exists (case sensitive), if so it is parsed for either or both headings: **Technologies** and **Features**.

This feature is used to generate bullet-point lists of project specifications on the site.

In the **README.md** of each project listed in `ProjectSpecs.js`, put the following markdown:

    ## Features

    - An awesome feature
    - Another awesome feature

    ## Technologies

    - React
    - Traefik

In each list, you can include as many items as you want.

**Notes:**

- This is not a robust feature yet, so avoid punctuation and special characters and keep to good markdown standards.
- The repository name must be **"main"**.

### Docker Compose: Traefik

**`docker-compose.yml`**

To use the Traefik production build, [you will need to set up a network and docker container for traefik](https://doc.traefik.io/traefik/v1.7/user-guide/docker-and-lets-encrypt/). By default the containers are expecting traefik to be running LetsEncrypt

[You will need to set up a network and docker container for traefik with LetsEncrypt](https://doc.traefik.io/traefik/v1.7/user-guide/docker-and-lets-encrypt/).

Once Traefik is up, run the following for a production build:

`docker-compose -f docker-compose.yml up -d --build`

This will make the app available to the URLs you've defined in the root dir `.env` file.

### Docker Compose: No-Traefik

**`docker-compose-no-traefik.yml`**

Run `docker-compose -f docker-compose.yml up -d --build` for a development build with live updates for the code. This will open the app in **localhost**.

#### Development

You must do the folllowing

- Localhost, with ports are selected
- Make sure you have app port variables set in the `.env` file
- You may have to make **"http"** the protocol for URL variables and arguments in the `docker-compose-no-traefik.yml` file.
- Make sure **"listen"** and **"server_name"** are configured in `client/nginx.conf` file

#### Production


If you don't want to use traefik you will have to make sure of the following

Run `docker-compose -f docker-compose-no-traefik.yml up -d --build`.

See docker-compose files and the referenced Dockerfiles for more details on each build.

## Credits

- [Jared long](https://dev.to/jlong4223) - [React.js, Nodemailer, and OAuth2](https://dev.to/jlong4223/how-to-implement-email-functionality-with-node-js-react-js-nodemailer-and-oauth2-2h7m)
- [React, NGinx and Docker 404](https://stackoverflow.com/questions/43555282/react-js-application-showing-404-not-found-in-nginx-server/61753597#61753597)
- [React Nginx router 404](https://medium.com/greedygame-engineering/so-you-want-to-dockerize-your-react-app-64fbbb74c217)
- [Reddis & Express.js](https://medium.com/geekculture/using-redis-with-docker-and-nodejs-express-71dccd495fd3)
