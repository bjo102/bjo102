import phpCmsImg from 'media/images/php_cms_featured.jpg'
import RSImg from 'media/images/react_img.jpg'
import BJOImg from 'media/images/bjo102_img.jpg'
import WHP from 'media/images/westival-home-page.jpg'
import CHHP from 'media/images/custom-house-home-page.jpg'
import DVHP from 'media/images/d-view-home-page.jpg'
import DMHP from 'media/images/d-mullin-home-page.jpg'

/**
 * Define the projects that appear on the projects component.
 * 
 *  A Project should have the following scheme:
 * 
 * - title (required)
 * - slug (required)
 * - source (required)
 * - id (optional|required for Gitlab)
 * - featuredImg (optional)
 * - technologies (optional)
 * - features (optional)
 * - readmeRaw (optional)
 * - devLog (optional)
 * 
 * Each property of this object is a project that will be queried form the remote gitlab/github repository.
 * Add additional features to each project as you wish by adding nested properties to the project.
 * 
 * The name of the project/each top level property of this object, must be a camelcase version of the slug of the project.
 * 
 * As an example, if you are pulling the git repo https://github.com/jaydenseric/graphql-react, then the project name in this object must be graphqlReact.
 * 
 * The project names listed here as top level properties are used to bind nested values to the response information when the remote repository is queried for other information.
 */
const ProjectSpecs = {
    phpCms : {
        source : 'gitlab',
        id : '23310513',
        featuredImg : phpCmsImg,
    },
    /* reactWebsite : {
        source : 'gitlab',
        id : '33871869',
        featuredImg : RSImg,
    }, */
    bjo102 : {
        source : 'gitlab',
        id : '33882754',
        featuredImg : BJOImg,
    },
    westival : {
        title : "Westival",
        description : "An events website, with events archives and a virtual ticket checkin system.",
        liveView : 'http://westival.ie',
        featuredImg : WHP,
        features : [
            'Ticketing system',
            'Custom AJAX archive',
            'Custom checkin system',
            'Custom events slider',
        ],
    },
    customHouse : {
        title : 'Custom House',
        description : 'A website for promoting exhibitions, artists and galleries.',
        liveView : 'https://www.customhousestudios.ie',
        featuredImg : CHHP,
        features : [
            'Artists & Exhibitions',
            'Embedded galleries & image slider modals',
            'Embedded entries of the Custom House Youtube channel',
        ],
    },
    dynamicalView : {
        title : 'Dynamical View',
        description : 'The website for Dynamical View, the web development branch of Cloud Daemons Ltd.',
        liveView : 'https://dynamicalview.net',
        featuredImg : DVHP,
        features : [
            'Custom Designed logo',
            'Custom Vectors & PNG images',
            'Javascript & CSS Animations',
            'Automated on-page article navigation',
            'Custom AJAX archive',
        ],
    },
    dermotMullin : {
        title: 'Dermot Mullin Locksmiths',
        description : 'A website for booking locksmithing services with Dermot Mullin.',
        liveView : 'https://dermotmullinlocksmith.ie',
        featuredImg : DMHP,
        features : [
            'Custom Designed logo',
            'Custom Vectors & PNG images',
            'A Booking form with priced services',
            'CTAs linked to the booking form',
            'Images edited in Adobe Photoshop'
        ],
    }
}

export default ProjectSpecs