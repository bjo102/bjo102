## Summary

Westival is an organisation which promotes events of various types in the town of Westport, Co.Mayo, Ireland.

This was made in 2021, when the **Irish national Covid-19 restrictions** were high, this led to Westival 2021 going in the direction of promoting virtual events.

## Requirements

- Events archive/calendar - Current and old.
- Information pages.
- Online event check-in system.