## Summary

Custom House Studios & Galleries are an organisation which hosts and promotes art exhibitions. They are based in Westport, Co. Mayo in Ireland.

A user of this site should have access to information on exhibitions and the featured artists. This would require custom post types and archives on the front page.

Presentation of featured artwork is also a key issue. A user would expect a clean presentation of works, annotated with titles, medium, dimensions and any other information related to the artworks.

## Requirements

- Easy access to information on exhibitions and artists.
- Expandible presentation of artworks.
- Location details of the Custom House Studio.
- Static pages for information on Custom House Studios & Galleries.