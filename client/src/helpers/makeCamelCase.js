/**
 * Convert any string to camel case, based on regex/string split value.
 * @param {string} str 
 * @param {*} regex 
 * @returns camelcased string 
 */
const makeCamelCase = (str, regex) => str.split(regex).map((e,i) => i ? e.charAt(0).toUpperCase() + e.slice(1).toLowerCase() : e.toLowerCase()).join('')
export default makeCamelCase