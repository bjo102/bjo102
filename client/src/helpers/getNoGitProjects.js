/**
 * 
 * @param {*} proj 
 * @return {array} the projects
 */
const getNoGitProjects = projs => {
    let entries = Object.entries(projs)
    let noGitProjs = entries.filter(([key, value]) => {
        if( value.source ) {
            return false
        }
        return {key: value}
    })
    return Object.fromEntries(noGitProjs)
}
export default getNoGitProjects