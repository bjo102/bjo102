/**
 * Get the slug of a given url.
 * @param {string} url 
 * @return slug
 */
const getUrlSlug = url => {
    let segments = new URL(url).pathname.match(/[^/]+/g)
    return segments[segments.length - 1]
}

export default getUrlSlug