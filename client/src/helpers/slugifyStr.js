/**
 * Prepares a string to be url and html friendly.
 * @param {string} str The string to be slugified.
 * @returns The slugified string.
 */
const slugifyStr = (str) => {
    str = str.replace(/^\s+|\s+$/gm, '')
    str = str.toLowerCase()
    
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;"
    var to   = "aaaaeeeeiiiioooouuuunc------"
    for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
    }
    
  str = str
    .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
    .replace(/\s+/g, "-") // collapse whitespace and replace by -
    .replace(/-+/g, "-") // collapse dashes
    .replace(/^-+/, "") // trim - from start of text
    .replace(/-+$/, ""); // trim - from end of text

    return str;
}

export default slugifyStr