/**
 * Get the remote data and return JSON object.
 * 
 * @param {string} url 
 * @param {*} query 
 * @param {*} variables 
 * @param {*} token 
 * @returns 
 */
const fetchGraphQL = async (url, query, variables, token) => {  
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Accept: "application/json",
            Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
            query: query,
            variables,
        }),
    });

    return await response.json();
}

export default fetchGraphQL;