import makeCamelCase from 'helpers/makeCamelCase';

const getFolioItemKeyFromUrl = () => {
    const theURL = new URL(window.location);
    const fItemSlug = theURL.pathname.replace('/portfolio/', '');
    const fItemKey = makeCamelCase(fItemSlug, /[-_]/)
    return fItemKey
}
export default getFolioItemKeyFromUrl