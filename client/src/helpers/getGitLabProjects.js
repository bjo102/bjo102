const getGitLabProjects = projs => {
    let entries = Object.entries(projs)
    let gitLabProjs = entries.filter(([key, value]) => {
        if( value.source !== 'gitlab' ) {
            return false
        }
        return {key: value}
    })
    return Object.fromEntries(gitLabProjs)
}

export default getGitLabProjects