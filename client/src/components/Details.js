import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faEnvelope, faLocationDot } from '@fortawesome/free-solid-svg-icons'
import { faLinkedin } from '@fortawesome/free-brands-svg-icons'

const Details = () => {
    return (
        <div className="contact-details-container">
            <h3>Details</h3>
            <ul className="contact-details-list">
                <li className="contact-detail"><FontAwesomeIcon icon={faUser} /> <span>Byron O'Malley</span></li>
                <li className="contact-detail"><FontAwesomeIcon icon={faLocationDot} /> <span>Galway, Ireland.</span></li>
                <li><a className="contact-detail" href="mailto:byronomalley950@gmail.com" tabIndex="0"><FontAwesomeIcon icon={faEnvelope} /> <span>byronomalley950@gmail.com</span></a></li>
                <li><a className="contact-detail" href="https://www.linkedin.com/in/byron-o-malley/" title="My LinkedIn Account" target="_blank" rel="noreferrer" tabIndex="0"><FontAwesomeIcon icon={faLinkedin} /> <span>linkedin.com/in/byron-o-malley</span></a></li>
            </ul>
        </div>
    )
}

export default Details