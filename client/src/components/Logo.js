import { ReactComponent as LogoSVG } from 'media/vectors/bjo102-logo.svg'
const Logo = () => <a className="site-logo-wrapper" href="/"><LogoSVG/></a>
export default Logo