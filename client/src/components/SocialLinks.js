import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGitlab, faLinkedin } from '@fortawesome/free-brands-svg-icons'

function SocialLinks() {
    return(
        <div className="social-links-container">
            <span>Find me</span>
            <a href="https://gitlab.com/bjo102" title="My Gitlab Account" target="_blank" rel="noreferrer" tabIndex="0"><FontAwesomeIcon icon={faGitlab} /></a>
            <a href="https://www.linkedin.com/in/byron-o-malley/" title="My LinkedIn Account" target="_blank" rel="noreferrer" tabIndex="0"><FontAwesomeIcon icon={faLinkedin} /></a>
        </div>
    )
}

export default SocialLinks