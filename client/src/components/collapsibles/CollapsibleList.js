import React, { useState } from 'react'

const CollapsiblesList = ({ children }) => {
    const [activeAria, setActiveAria] = useState(null)
    
    return (
        <ul className="collapsable-list">
            {children}
        </ul>
    )
}

export default CollapsiblesList