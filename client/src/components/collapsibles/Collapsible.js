import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus, faMinus } from '@fortawesome/free-solid-svg-icons'

const Collapsible = ({ heading, children }) => {
    const [expanded, setExpanded] = useState(false)
    const toggleExpanded = () => setExpanded(!expanded)
    const content = expanded ? <div className="collapsible-content">{children}</div> : ''
    const icon = expanded ? faMinus : faPlus
    const ariaExp = expanded ? 'true' : 'false'
    
    return (
        <article className="collapsible-item" aria-expanded={ariaExp}>
            <button className="collapsible-toggle" onClick={() => toggleExpanded()}>
                <h3 className="heading">{heading}</h3> <FontAwesomeIcon className="fa-icon" icon={icon}/>
            </button>
            {content}
        </article>
    )
}

export default Collapsible