import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGear, faCirclePlus } from '@fortawesome/free-solid-svg-icons'

const Specs = ({ features, technologies, languages}) => {

    const [hasItems, setHasItems] = useState(false)
    const [feats, setFeat] = useState([])
    const [techs, setTech] = useState([])
    const [langs, setLangs] = useState({})
    
    const List = ({ items }) => <ul className="project-specs-list">{items.map((t, key) => <li key={key}>{t}</li>)}</ul>

    const LangList = ({ langs }) => {
        const keys = Object.keys(langs)
        const langItems = keys.map((lang, key) => {
            return (
                <li key={key} data-percentage={langs[lang]}><strong>{lang}</strong> {langs[lang]}%</li>
            )
        })
        return <ul className="project-specs-list">{langItems}</ul>
    }

    const ListCont = ({ heading, children }) => {
        if (children) {
            return (
                <div className="project-specs-list-container">
                    <h4 className="heading">{heading}</h4>
                    {children}
                </div>
            )
        } else {
            return '';
        }
    }

    const FeatList = () => {
        if (feats.length) {
            return (
                <>
                <ListCont heading="Features">
                    <List items={feats} />
                </ListCont>
                <FontAwesomeIcon icon={faCirclePlus} />
                </>
            )
        } else {
            return ''
        }
    }

    const TechList = () => {
        if (techs.length || Object.keys(langs).length) {
            const techlist = techs.length ? <ListCont heading="Technologies"><List items={techs} /></ListCont> : ''
            const langList = Object.keys(langs).length ? <ListCont heading="Languages"><LangList langs={langs} /></ListCont> : ''
            return (
                <>
                {techlist}
                {langList}
                <FontAwesomeIcon icon={faGear} />
                </>
            )
        } else {
            return ''
        }
    }

    useEffect(() => {
        let fs = features ? features : []
        let ts = technologies ? technologies : []
        let ls = languages ? languages : {}
 
        if(fs.length > 0 | ts.length > 0 | Object.keys(ls).length > 0) {
            setHasItems(true)
            if (fs.length > 0) {
                setFeat(fs)
            }
            if (ts.length > 0) {
                setTech(ts)
            }
            if (Object.keys(ls).length > 0) {
                setLangs(ls)
            }
        }
    }, [features, technologies, languages])

    if (hasItems) {
        return (
            <div className="project-page-specs-container">
                <aside className="project-specs project-features-specs">
                    <FeatList/>
                </aside>
                <aside className="project-specs project-tech-specs">
                    <TechList/>
                </aside>
            </div>
        )
    } else {
        return ''
    }

}

export default Specs