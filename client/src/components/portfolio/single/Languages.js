const Languages = ({langs}) => langs.map((lang, key) => <li className="project-language" key={key}>{lang}</li>)
export default Languages