import React, { useContext } from 'react';
import { AppContext } from 'context'

const FolioNav = ({ currentIndex }) => {
    const {Projects} = useContext(AppContext);
    let projArr = Object.keys(Projects);
    let nextItem = currentIndex < (projArr.length - 1) ? projArr[currentIndex + 1] : projArr[0];
    let prevItem = currentIndex > 0 ? projArr[currentIndex - 1] : projArr[projArr.length - 1];
    let nextItemLink = Projects[nextItem].slug;
    let prevItemLink = Projects[prevItem].slug;

    return (
        <nav>
            <a className="btn" href={`/portfolio/${prevItemLink}`} tabIndex="0">Previous Item</a>
            <a className="btn" href={`/portfolio/${nextItemLink}`} tabIndex="0">Next Item</a>
            <a className="btn" href="/portfolio/" tabIndex="0">Back to Archive</a>
        </nav>
    )
}

export default FolioNav