import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye } from '@fortawesome/free-solid-svg-icons'

/**
 * The link to the info page for the portfolio item.
 * 
 * @param {string} slug The last segment of the url which identifies the page.  
 * @returns {JSX Element} The link to the portfolio info page.
 */
const LiveLink = ({ url }) => url ? <a href={url} tabIndex="0" target="_blank" rel="noreferrer"><FontAwesomeIcon icon={faEye}/></a> : ''

export default LiveLink