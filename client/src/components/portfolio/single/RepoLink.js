import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGitlab, faGithub } from '@fortawesome/free-brands-svg-icons'

const RepoLink = ({link, source }) => {
    if (!link || !source) return ''

    let src = source ? source : 'github'
    let icon = ''

    if (src === 'gitlab') {
        icon = <FontAwesomeIcon icon={faGitlab} /> 
    } else if (src === 'github') {
        icon = <FontAwesomeIcon icon={faGithub} /> 
    } else {
        return ''
    }

    return <a href={link} tabIndex="0" target="_blank" rel="noreferrer">{icon}</a>
}

export default RepoLink