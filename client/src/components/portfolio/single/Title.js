const Title = ({ title, link }) => {
    if (!title) return ''
    
    const t = <h3 className="heading portfolio-item-heading">{title}</h3>
    return link ? <a href={link} target="_blank" rel="noreferrer" tabIndex="0">{t}</a> : t
}
export default Title