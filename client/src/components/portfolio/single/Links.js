
import RepoLink from 'components/portfolio/single/RepoLink'
import PageLink from 'components/portfolio/single/PageLink'
import LiveLink from 'components/portfolio/single/LiveLink'

const Links = ({slug, repoLink, liveView, source}) => {
    return (
        <div className="portfolio-links">
            <PageLink slug={slug} />
            <RepoLink link={repoLink} source={source} />
            <LiveLink url={liveView} />
        </div>
    )
}

export default Links