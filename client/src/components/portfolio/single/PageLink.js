import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom'

/**
 * The link to the info page for the portfolio item.
 * 
 * @param {string} slug The last segment of the url which identifies the page.  
 * @returns {JSX Element} The link to the portfolio info page.
 */
const PageLink = ({ slug }) => slug ? <Link to={`/portfolio/${slug}`} tabIndex="0"><FontAwesomeIcon icon={faInfoCircle}/></Link> : ''

export default PageLink