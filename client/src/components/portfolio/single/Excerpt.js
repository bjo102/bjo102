const Excerpt = ({ excerpt, max }) => excerpt && max ? <p className="excerpt">{excerpt.length <= max ? excerpt : `${excerpt.substr(0, max)}...`}</p> : ''
export default Excerpt