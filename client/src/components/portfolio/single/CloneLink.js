const CloneLink = () => {
    if(repoLink) {
        return (
            <div className="clipboard-link-container">
                <label htmlFor={`${slugifyStr(title)}-clipboard-link-clone-repo-https`}>Clone repo with https</label>
                <input id={`${slugifyStr(title)}-clipboard-link-clone-repo-https`} className="clipboard-link clpbrd-lnk-http-clone-url" value={repoLink} onClick={(e) => e.target.select()} type="text" readOnly="readonly" aria-label="Repository clone URL" />
                <button onClick={(e) => clipBoardOnClick(e.target, repoLink)} className="btn clipboard-link-btn" type="button" title="Copy URL" aria-label="Copy URL" aria-live="polite">
                    <FontAwesomeIcon icon={faCopy}/>
                    <span className="copied-tooltip">Copied to clipboard</span>
                </button>
            </div>
        )
    } else {
        return ''
    }

    function clipBoardOnClick(button, repoLink) {
        navigator.clipboard.writeText(repoLink)
        button.classList.add('clicked')
        setTimeout(() => button.classList.remove('clicked'), 2000)
    }
}

export default CloneLink