import React, { useState, useEffect, useContext } from 'react';
import { AppContext } from 'context'
import ReactMarkdown from 'react-markdown'

import Header from 'components/temp/Header'
import Specs from 'components/portfolio/single/Specs';
import RepoLink from 'components/portfolio/single/RepoLink';
import LiveLink from 'components/portfolio/single/LiveLink';
import getFolioItemKeyFromUrl from 'helpers/getFolioItemKeyFromUrl';

// import FolioNav from 'components/portfolio/single/FolioNav';

const FolioPageContent = () => {
    const {Projects} = useContext(AppContext);
    const key = getFolioItemKeyFromUrl()
    const project = Projects[key]
    const [langs, setLangs] = useState(null)
    const [content, setContent] = useState(null)
    const title = project.title
    const source = project.source
    const repoLink = project.repoLink
    const afterHeading = <><p>{project.description}</p></>
    const tech = project.technologies
    const features = project.features
    const liveView = project.liveView
    //const currentIndex = Object.keys(Projects).findIndex((k) => k === key)

    useEffect(() => {
        // domain locations
        if (Projects === undefined) return

        const gitlabDevLog = `${process.env.REACT_APP_SERVER_URL}folio-item?id=${project.id}`
        const localDevLog = `${process.env.REACT_APP_SERVER_URL}devLog?project=${key}`
        
        if (source === 'gitlab') {
            fetch(gitlabDevLog)
            .then(response => response.json())
            .then(data => {
                setContent(data.wiki.content)
                setLangs(data.langs)
            })
        } else {
            fetch(localDevLog)
            .then(response => response.text()) 
            .then(res => {
                setContent(res)
            }).catch(err => { throw new Error(`There was an issue: ${err.message}`)})
        }
    }, [Projects, source])

    return (
        <>
        <Header heading={title}>{afterHeading}</Header>
        <section className="section folio-page-section">
            <Specs features={features} technologies={tech} languages={langs} />
            <div id="folio-page-content-wrapper">
                <div id="folio-page-content">
                    <ReactMarkdown>{content}</ReactMarkdown>
                </div>
                <div id="folio-page-links-wrapper">
                    <div id="folio-page-links">
                        <RepoLink link={repoLink} source={source} />
                        <LiveLink url={liveView} />
                    </div>
                </div>

            </div>
            {/* Broken <FolioNav currentIndex={currentIndex}/> */}
        </section>
        </>
    )
}
export default FolioPageContent