import React, { useState, useEffect, useContext } from 'react';
import { AppContext } from 'context'

import Loader from 'components/Loader'
import FourOFour from 'components/pages/FourOFour'
import FolioPageContent from 'components/portfolio/FolioPageContent';
import getFolioItemKeyFromUrl from 'helpers/getFolioItemKeyFromUrl';

const FolioPage = () => {
    const {Projects} = useContext(AppContext)
    const [project, setProject] = useState(null)
    const [isLoading, setLoading] = useState(true)
    const fItemKey = getFolioItemKeyFromUrl()

    useEffect(() => {
        if (Projects === undefined) return
        if(Projects[fItemKey]) {
            setProject(Projects[fItemKey])
        }
        setLoading(false)
    }, [fItemKey, Projects])

    if (isLoading) {
        return <Loader/>
    } else if (project) {
        return <FolioPageContent />
    } else {
        return <FourOFour/>
    }
}

export default FolioPage