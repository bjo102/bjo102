import React from 'react';
import Title from 'components/portfolio/single/Title'
import Links from 'components/portfolio/single/Links'
import Excerpt from 'components/portfolio/single/Excerpt'


const FolioArchiveItem = ({ proj }) => {
    const title = proj.title
    const link = proj.link
    const liveView = proj.liveView
    const fImg = proj.featuredImg
    const slug = proj.slug
    const source = proj.source

    const repoLink = proj.repoLink
    const excerpt = proj.description
    //const readme = proj.readme_url
    //const[showCopied, setShowCopied] = useState(false)

    const backgrndStyle = {
        background: `url(${fImg})`,
        backgroundPosition: 'center',
        objectFit: 'contain',
        backgroundSize: 'cover'
    }
    
    return (
        <article className="portfolio-archive-item" style={backgrndStyle}>
            <div className="portfolio-item-dtls">
                <Title title={title} link={link} />
                <Excerpt excerpt={excerpt} max={100} />
                <Links slug={slug} repoLink={repoLink} source={source} liveView={liveView} />
            </div>
        </article>
    )
}

export default FolioArchiveItem