// import {ReactComponent as LoaderSVG} from 'media/vectors/loader.svg'

const Loader =() => {
    return (
        <section className="section loader"><h1>Loading...</h1></section>
    )
}

export default Loader