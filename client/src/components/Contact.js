import React, { useState } from 'react';
import validator from 'validator'
import ReCAPTCHA from "react-google-recaptcha";

/**
 * The contact form component.
 * 
 * This form sends data to the proxy server, to then be validated and sent to
 * the email address if the validation is successful.
 * 
 * @returns {JSX Element} 
 */
const Contact = () => {
    const [inputState, setInputState] = useState({
        name: "",
        email: "",
        organization: "",
        message: "",
    });

    const [errors, setErrs] = useState({
        name: "",
        email: "",
        organization: "",
        message: "",
        recaptcha: "",
    })

    /**
     * Handle input value changes.
     * 
     * @since 1.0.0
     * 
     * @param {object} e The event handler object.
     * @return void
     */
     const handleInputChange = e => {
        setInputState((prevState) => ({
            ...prevState,
            [e.target.name]: e.target.value,
        }));
    }

    const [formNotice, setFormNotice] = useState('')
    const [formNoticeClass, setFormNoticeClass] = useState('form-success')

    /**
     * The URL to the proxy server.
     * 
     * This contains the trailing slash.
     * 
     * @since 1.0.1
     * 
     * @var {string}
     */
    const serverURL = process.env.REACT_APP_SERVER_URL

    /**
     * The reCAPTCHA site key.
     * 
     * @since 1.0.1
     * 
     * @var {string}
     */
    const recaptchaSiteKey = process.env.REACT_APP_RECAPTCHA_SITE_KEY

    /**
     * The reCAPTCHA form input reference.
     * 
     * @since 1.0.1
     * 
     * @var {object}
     */
    const recaptchaRef = React.createRef();

    /**
     * Print an error message.
     * 
     * @since 1.0.1
     * 
     * @param {string} err The error message. 
     * @returns {JSX} The error message JSX.
     */
    const Err = ({ children }) => children ? <p className="form-input-err-notice">{children}</p> : ''

    /**
     * Get the form notice text.
     * 
     * This will notify whether or not the email was sent.
     * 
     * @since 1.0.0
     * 
     * @return {JSX Element}
     */
    const FormNotice = () => formNotice ? <p className={formNoticeClass}>{formNotice}</p> : ''

    /**
     * Empty all input values.
     * 
     * @since 1.0.0
     * 
     * @return void
     */
    const cleanInputs = () => {
        setInputState({
            name: '',
            email: '',
            organization: '',
            message: ''
        });
    }

    /**
     * Handle contact form submissions.
     * 
     * @since 1.0.0
     * 
     * @param {object} e The event handler object.
     * @return void
     */
    async function submitEmail(e) {
        e.preventDefault();

        let errs = {
            name: "",
            email: "",
            organization: "",
            message: "",
            recaptcha: "",
        }

        const isValidSubmission = () => Object.values(errs).every(err => err.length == 0)
        
        const recaptchaValue = recaptchaRef.current.getValue()

        const mailerState = {
            ...inputState,
            recaptchaValue: recaptchaValue
        }

        const handleServerResponse = async res => {
            const resData = await res;
            if (resData.status === "success") {
                setFormNotice(`Thank you for your message ${mailerState.name}! Your email has been sent.`)
                setFormNoticeClass('form-success')
                setTimeout(() => setFormNotice(''), 5000)
            } else if (resData.status === "fail") {
                setFormNotice(`Sorry ${mailerState.name}, your message was not sent.`)
                setFormNoticeClass('form-failure')
                setTimeout(() => setFormNotice(''), 5000)
            }
        }
    
        const handleServerConErr = error => {
            setFormNotice('Sorry, there was a connection error with the server.')
            setFormNoticeClass('form-failure')
            throw new Error(`There was an issue: ${error.message}`)
        }

        if (!validator.isEmail(mailerState.email)) {
            errs.email = 'This must be a valid email address.'
        }

        if (validator.isEmpty(recaptchaValue)) {
            errs.recaptcha = 'Check the reCAPTCHA field.'
        }

        recaptchaRef.current.reset()

        if (isValidSubmission()) {

            if(mailerState.organization) {
                mailerState.message = `${mailerState.message}\nOrganization: ${mailerState.organization}`;
            }

            let header = {
                method: "POST",
                headers: {
                    "Content-type": "application/json",
                },
                body: JSON.stringify({ mailerState }),
            }

            await fetch(`${serverURL}send`, header)
                .then(response => response.json())
                .then(handleServerResponse)
                .then(cleanInputs)
                .catch(handleServerConErr)
        }

        setErrs(errs)
    };

    return (
        <form className="contact-form" onSubmit={submitEmail}>
            <header>
                <h3>Contact</h3>
                <FormNotice />
            </header>
            <fieldset className="form-body">
                <div className="field-container contact-name-container">
                    <label htmlFor="contact-name">Name <span className="required-asterisk">*</span></label>
                    <input id="contact-name" name="name" type="text" value={inputState.name} onChange={handleInputChange} maxLength="100" aria-required="true" required />
                    <Err>{errors.name}</Err>
                </div>
                <div className="field-container contact-organization-container">
                    <label htmlFor="contact-organization">Organization</label>
                    <input id="contact-organization" name="organization" type="text" value={inputState.organization} onChange={handleInputChange} maxLength="100" />
                    <Err>{errors.organization}</Err>
                </div>
                <div className="field-container contact-email-container">
                    <label htmlFor="contact-email">Email <span className="required-asterisk">*</span></label>
                    <input id="contact-email" name="email" type="email" value={inputState.email} onChange={handleInputChange} maxLength="100" aria-required="true" required />
                    <Err>{errors.email}</Err>
                </div>
                <div className="field-container contact-message-container">
                    <label htmlFor="contact-message">Message <span className="required-asterisk">*</span></label>
                    <textarea id="contact-message" name="message" value={inputState.message} onChange={handleInputChange} maxLength="500" aria-required="true" required />
                    <Err>{errors.message}</Err>
                </div>
            </fieldset>
            <div className="form-footer">
                <ReCAPTCHA ref={recaptchaRef} sitekey={recaptchaSiteKey} theme="dark" />
                <Err>{errors.recaptcha}</Err>
                <input className="btn submit" type="submit" value="Message Me" />
            </div>
        </form>
    );

}

export default Contact