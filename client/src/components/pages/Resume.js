import SubSection from 'components/sections/SubSection'
import ProfileImg from 'media/images/byron-profile-official.jpg'
import Collapsible from 'components/collapsibles/Collapsible'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGitlab, faLinkedin } from '@fortawesome/free-brands-svg-icons'
import { faLocationPin } from '@fortawesome/free-solid-svg-icons'

const ResumeHeader = () => {
    return(
        <section id="resume-header" className="section dark">
            <div>
                {/* <img id="resume-profile-img" src={ProfileImg} /> */}
                <div className="details">
                    <h1>Byron J. O'Malley</h1>
                    <p>Front-End Web Developer.</p>
                    <a href="https://gitlab.com/bjo102" title="My Gitlab Account" target="_blank" rel="noreferrer" tabIndex="0">
                        <FontAwesomeIcon icon={faGitlab} />
                        gitlab.com/bjo102
                    </a><br/>
                    <a href="https://www.linkedin.com/in/byron-o-malley-0850b217b/" title="My Gitlab Account" target="_blank" rel="noreferrer" tabIndex="0">
                        <FontAwesomeIcon icon={faLinkedin} />
                        linkedin.com/in/byron-o-malley-0850b217b
                    </a><br/>
                    <p><FontAwesomeIcon icon={faLocationPin} />Galway, Ireland.</p>
                </div>
            </div>
        </section>
    )
}

const Summary = () => {
    return (
        <section id="cv-summary" className="section">
            <h2 className="sect-heading heading">Summary</h2>
            <p>
                Web developer with experience in delivering solutions for various needs. Backed by knowlegde and code libraries developed over 
                varous projects, and a proven ability to execute on design, communication, adminsistration, continuous assessment and improvement, engagement in devops 
            </p>
        </section>
    )
}

const Education = () => {
    return(
        <section id="cv-education" className="section">
            <h2 className="sect-heading heading">Education</h2>
            <ul className="collapsible-list">
                <li>
                    <Collapsible heading="Louisburgh National School">
                        <p>Louisburgh, Co. Mayo, Ireland.</p>
                        <p>Primary School.</p>
                        <p><time dateTime="1998">1998</time> - <time dateTime="2002">2002</time></p>
                    </Collapsible>
                </li>

                <li>
                    <Collapsible heading="Ysgol Bro Gwydir">
                        <p>Llanrwst, Conwy County, Wales.</p>
                        <p>Primary School.</p>
                        <p><time dateTime="2002">2002</time> - <time dateTime="2005">2005</time></p>
                    </Collapsible>
                </li>

                <li>
                    <Collapsible heading="Ysgol Dyffryn Conwy">
                        <p>Llanrwst, Conwy County, Wales.</p>
                        <p>Secondary School.</p>
                        <p><time dateTime="2005">2005</time> - <time dateTime="2010">2010</time></p>
                    </Collapsible>
                </li>

                <li>
                    <Collapsible heading="Rice College Westport">
                        <p>Westport, Co. Mayo, Ireland.</p>
                        <p>Secondary School.</p>
                        <p><time dateTime="2010">2010</time> - <time dateTime="2012">2012</time></p>
                    </Collapsible>
                </li>

                <li>
                    <Collapsible heading="Westport College of Further Education">
                        <p>Westport, Co. Mayo, Ireland.</p>
                        <p>Further Education and Training Awards Council (FETAC).</p>
                        <p><time dateTime="2012">2012</time> - <time dateTime="2013">2013</time></p>
                    </Collapsible>
                </li>

                <li>
                    <Collapsible heading="Limerick School of Art & Design">
                        <p>Limerick City, Ireland.</p>
                        <p>Ordinary Degree.</p>
                        <p>Ceramics Design.</p>
                        <p><time dateTime="2014-9">September, 2014</time> - <time dateTime="2017-5">May, 2017</time></p>
                    </Collapsible>
                </li>
            </ul>
        </section>
    )
}

const WEXP =() => {
    return(
        <section id="cv-work-experience" className="section">
            <h2 className="sect-heading heading">Work Experience</h2>

            <ul className="collapsible-list">
                <li>
                    <Collapsible heading="Ty Hwnt Ir Bont">
                        <p>Llanrwst, Co. Conway, Wales.</p>
                        <p>Kitchen Porter.</p>
                        <p><time dateTime="2008">2008</time> - <time dateTime="2009">2009</time></p>
                    </Collapsible>
                </li>

                <li>
                    <Collapsible heading="Enotica Italian Restaurant">
                        <p>Westport, Co. Mayo, Ireland.</p>
                        <p>Kitchen Porter.</p>
                        <p><time dateTime="2011">2011</time> - <time dateTime="2012">2012</time></p>
                    </Collapsible>
                </li>

                <li>
                    <Collapsible heading="Mangos Seafood Restaurant">
                        <p>Westport, Co. Mayo, Ireland.</p>
                        <p>Kitchen Porter.</p>
                        <p><time dateTime="2012">2012</time> - <time dateTime="2013">2013</time></p>
                    </Collapsible>
                </li>

                <li>
                    <Collapsible heading="Agricultural properties in Norway">
                        <p>Porsgrunn, Norway.</p>
                        <p>Strawberry harvesting.</p>
                        <p><time dateTime="2014-6">June 2014</time> - <time dateTime="2014-7">July 2014</time></p>
                    </Collapsible>
                </li>

                <li>
                    <Collapsible heading="The Irish Rover">
                        <p>Puerto Del Carment, Lanzarote.</p>
                        <p>Bar service.</p>
                        <p><time dateTime="2015-7">July 2015</time> - <time dateTime="2015-9">September 2015</time></p>
                    </Collapsible>
                </li>

                <li>
                    <Collapsible heading="Kingsbridge Music Festival 2016">
                        <p>Kingsbridge, England.</p>
                        <p>Bar service.</p>
                        <p><time dateTime="2016">2016</time></p>
                    </Collapsible>
                </li>

                <li>
                    <Collapsible heading="PhoneWatch Home Security / Prepay Power / SSE Airtricity">
                        <p>Co. Limerick & Co. Clare, Ireland.</p>
                        <p>Door-to-door sales.</p>
                        <p><time dateTime="2017">May 2017</time> - <time dateTime="2018">March 2018</time></p>
                    </Collapsible>
                </li>

                <li>
                    <Collapsible heading="Titan Sports">
                        <p>Limerick City, Ireland.</p>
                        <p>Factory Packaging.</p>
                        <p><time dateTime="2017">March 2018</time> - <time dateTime="2018">September 2018</time></p>
                    </Collapsible>
                </li>

                <li>
                    <Collapsible heading="The Cornstore Restaurant">
                        <p>Limerick City, Ireland.</p>
                        <p>Kitchen Porter.</p>
                        <p><time dateTime="2017">September 2017</time> - <time dateTime="2018">October 2018</time></p>
                    </Collapsible>
                </li>

                <li>
                    <Collapsible heading="Cloud Daemons Ltd">
                        <p>Remote, Ireland.</p>
                        <p>Web development.</p>
                        <p><time dateTime="2017">October 2018</time> - <time dateTime="2018">June 2022</time></p>
                    </Collapsible>
                </li>
            </ul>
        </section>
    )
}

const Skills = () => {
    return(
        <section id="cv-skills" className="section">
            <h2 className="sect-heading heading">Skills</h2>
            <article>
                <h3>PHP</h3>
                <p>Being the language of WordPress makes this the one to be aquainted with.</p>
                <ul>
                    <li>Building shortocodes</li>
                    <li>Developing Helper Classes</li>
                    <li>MVC architectures</li>
                    <li>Custom WP Queries</li>
                    <li>REST API requests</li>
                    <li>The native APIs of WordPress</li>
                    <li>Document parsing, editing and creation</li>
                </ul>
                <ul>
                    <li>PHP CMS</li>
                </ul>
            </article>
            <ul>
                <li>PHP</li>
                <li>JavaScript</li>
                <li>WordPress</li>
                <li>React</li>
                <li>SCSS</li>
                <li>Git</li>
                <li>Docker</li>
            </ul>
        </section>
    )
}

const Resume = () => {

    const cvScrollTo = id => document.getElementById(id).scrollIntoView({ behavior: "smooth" })
    return (
        <>
            <ResumeHeader/>
            <main id="main-content" className="resume-page">
                <div id="resume">
                    <Summary/>
                    <Skills/>
                    <Education/>
                    <WEXP/>
                    <section id="cv-references" className="section">
                        <h2 className="sect-heading heading">References</h2>
                        <SubSection heading="Shane Scott - Director at Cloud Daemons Ltd">
                            <p>(087 350 6163)</p>
                        </SubSection>
                    </section>
                </div>
                <aside id="resume-nav-container">
                    <nav id="resume-nav" className="nav-menu">
                        <ul className="menu-el menu-items-list">
                            <li><button className="btn" onClick={() => cvScrollTo('cv-summary')} tabIndex="0">Summary</button></li>
                            <li><button className="btn" onClick={() => cvScrollTo('cv-education')} tabIndex="0">Education</button></li>
                            <li><button className="btn" onClick={() => cvScrollTo('cv-work-experience')} tabIndex="0">Work Experience</button></li>
                            <li><button className="btn" onClick={() => cvScrollTo('cv-skills')} tabIndex="0">Skills</button></li>
                            <li><button className="btn" onClick={() => cvScrollTo('cv-references')} tabIndex="0">References</button></li>
                        </ul>
                    </nav>
                </aside>
            </main>
        </>
    )
}

export default Resume