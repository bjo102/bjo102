import React, { useContext } from 'react';
import { AppContext } from 'context';
import Header from 'components/temp/Header';
import FolioArchiveItem from 'components/portfolio/FolioArchiveItem';
import getGitLabProjects from 'helpers/getGitLabProjects';
import getNoGitProjects from 'helpers/getNoGitProjects';

const Portfolio = () => {
    const { Projects } = useContext(AppContext);
    if (Projects === undefined) return ''
    const gitProjects = getGitLabProjects(Projects)
    const noGitProject = getNoGitProjects(Projects)

    return (
        <>
        <Header heading="Portfolio"></Header>
        <section className="section portfolio-section">
            <h2>GitLab Projects</h2>
            <div className="section-content-wrapper">
                {Object.keys(gitProjects).map((key, i) => <FolioArchiveItem key={i} proj={gitProjects[key]}/>)}
            </div>
        </section>
        <section className="section dark portfolio-section">
            <h2>Cloud Daemons Internal Projects</h2>
            <p>
                These projects are live websites developed by Cloud Demons Ltd.
                The git repositories are not available to the public, but summaries have been written on each project.
            </p>
            <p>
                There have been many other projects, but due to non-disclosure agreements, they cannot be listed here.
            </p>
            <div className="section-content-wrapper">
                {Object.keys(noGitProject).map((key, i) => <FolioArchiveItem key={i} proj={noGitProject[key]}/>)}
            </div>
        </section>
        </>
    )
}

export default Portfolio