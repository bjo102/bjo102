function FourOFour() {
    return (
        <main id="main-content" className="section four-o-four-section">
            <h1 id="four-o-four-title">404</h1>
            <p>Webpage not found</p>
            <a href="/" className="btn">Go to Home Page</a>
        </main>
    )
}

export default FourOFour