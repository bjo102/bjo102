import React from 'react';
import LandingHeader from 'components/sections/LandingHeader';
import Services from 'components/sections/Services'
import About from 'components/sections/About'

const Home = () => {
    return (
        <>
        <LandingHeader/>
        <main id="main-content">
            <Services/>
            <About/>
        </main>
        </>
    );
}

export default Home