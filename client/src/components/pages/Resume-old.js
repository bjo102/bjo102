const skills = [
    'WordPress',
    'PHP',
    'JavaScript',
    'Git',
    'Docker',
    'React',
    'SCSS',
    'JQuery',
]

const interests = [
    'Art',
    'Reading',
    'Jiu-Jitsu',
    'Weight Lifting',
]

const AsideList = ({ heading, items }) => {
    return (
        <section className="aside-section">
            <h2 className="cv-heading aside-heading">{heading}</h2>
            <ul id="aside-skills-list" className="aside-list">
                {items.map((item, key) => <li className="aside-data-item aside-list-item" key={key}>{item}</li>)}
            </ul>
        </section>
    )
}

const Aside = () => {
    return (
        <aside id="aside-column" className="cv-column">
            <AsideList heading="Contact" items={[
                <p className="aside-data-item"><em>Email:</em> byronomalley950@gmail.com</p>,
                <p className="aside-data-item"><em>Phone:</em> +353 87 162 7705</p>
            ]} />
            <AsideList heading="Skills" items={skills} />
            <AsideList heading="Interests" items={interests} />
        </aside>
    )
}

const Section = ({ heading, children }) => {
    return (
        <section className="cv-section">
            <h2 className="cv-heading">{heading}</h2>
            {children}
        </section>
    )
}
const Education = () => {
    return (
        <Section heading="Education">
            <article className="cv-entry">
                <header>
                    <h3 className="cv-heading">Limerick School of Art & Design</h3>
                    <p>Limerick City, Ireland.</p>
                    <p>Ordinary Degree.</p>
                    <p><time dateTime="2014-9-1">September 1st, 2014</time> - <time dateTime="2017-5-1">May 1st, 2017</time></p>
                </header>
            </article>
        </Section>
    )
}

const Experience = () => {
    return (
        <Section heading="Experience">
            <article className="cv-entry">
                <header>
                    <h3 className="cv-heading">Cloud Daemons Ltd</h3>
                    <p>Westport, Co Mayo, Ireland</p>
                    <p><time dateTime="2018-10-1">October 1st, 2018</time> - Present</p>
                </header>
                <ul className="tasks">
                    <li>Provided front-end website development using WordPress, Third-party & custom software solutions.</li>
                    <li>Conducted testing and review of website design for responsiveness, clarity and effectiveness.</li>
                    <li>Used Gitlab as the project management tools to address client issues & requests, as well as code repositories.</li>
                    <li>Managed docker containers.</li>
                    <li>At times worked with Adobe software; Photoshop, Illustrator, XD.</li>
                </ul>
            </article>
        </Section>
    )
}

const References = () => {
    return (
        <Section heading="References">
            <article>
                <p>Shane Scott - Director at Cloud Demons Ltd</p>
                <ul>
                    <li>+353 87 350 3961</li>    
                    <li>shane@clouddemons.net</li>
                </ul>
            </article>
        </Section>
    )
}

const Resume = () => {
    return (
        <div id="resume-page">
            <header id="cv-header">
                <hgroup>
                    <h1 id="name" className="cv-heading">Byron O'Malley</h1>
                    <h2 id="title" className="cv-heading">Web Developer</h2>
                </hgroup>
            </header>
            <main id="main-content">
                <Aside/>
                <div id="main-column" className="cv-column">
                    <Education />
                    <Experience />
                    <References />
                </div>
            </main>
        </div>
    )
}
export default Resume