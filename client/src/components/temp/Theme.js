// ui components
import Navbar from 'components/temp/Navbar';
import Footer from 'components/temp/Footer'

const Theme = ({ children}) => {
    return (
        <div id="App">
            <Navbar />
            <div id="content">
                {children}
                <Footer />
            </div>
        </div>
    )
}

export default Theme