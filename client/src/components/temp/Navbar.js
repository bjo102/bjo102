import React, { useState, useEffect } from 'react';
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faXmark, faBars } from '@fortawesome/free-solid-svg-icons'
//import ResumePDF from 'media/downloads/BJO102.pdf'
import SocialLinks from 'components/SocialLinks'
import Logo from 'components/Logo'

const Navbar = () => {
    const [showMenu, setShowMenu] = useState(false)
    const [pastHeader, setPastHeader] = useState(false)
    const displayClass = showMenu ? 'visible' : 'hidden'
    const tglBtnClass = showMenu ? 'close' : 'open'
    const tglBtnContent = showMenu ? <FontAwesomeIcon icon={faXmark} /> : <FontAwesomeIcon icon={faBars} />
    const b2t = () => {
        closeMenu()
        document.querySelector('body').scrollIntoView({behavior: "smooth"})
    }
    const B2tButton = pastHeader ? <button className="menu-el btn back-to-top" onClick={b2t}>Back to top</button> : ''

    useEffect(() => {
        window.addEventListener('scroll', () => {
            if (window.scrollY > 50) {
                setPastHeader(true)
            } else {
                setPastHeader(false)
            }
        })
    }, [])
    
    //const [pastHeader, setPastHeader] = useState(false)
    //const location = useLocation();
    //const currentRoute = location.pathname

    const toggleMenu = () => {
        if (showMenu) {
            closeMenu()
        } else {
            openMenu()
        }
    }

    function openMenu() {
        setShowMenu(true)
        window.addEventListener('click', menuWindowChecker)
    }

    function closeMenu() {
        setShowMenu(false)
        window.removeEventListener('click', menuWindowChecker)
    }

    function menuWindowChecker(e) {
        let navbarBoundary = document.querySelector('#app-navbar').getBoundingClientRect()
        if (e.y > navbarBoundary.bottom || e.y < navbarBoundary.top) {
            closeMenu()
        }
    }

    return (
        <nav id="app-navbar" className={displayClass}>
            <button id="toggle-menu" className={tglBtnClass} onClick={toggleMenu}>{tglBtnContent}</button>
            <div className="nav-menu">
                <ul className="menu-el menu-items-list">
                    <li onClick={closeMenu}><Logo/></li>
                    <li><NavLink to="/" className={({ isActive }) => (isActive ? 'btn active' : 'btn inactive')} onClick={closeMenu} tabIndex="0">Home</NavLink></li>
                    <li><NavLink to="/portfolio" className={({ isActive }) => (isActive ? 'btn active' : 'btn inactive')} onClick={closeMenu} tabIndex="0">Portfolio</NavLink></li>
                    <li><button className="btn" onClick={() => {
                        closeMenu();
                        document.getElementById('footer').scrollIntoView({behavior: "smooth"})
                    }} tabIndex="0">Contact</button></li>
                    <SocialLinks />
                </ul>
                {B2tButton}
            </div>
        </nav>
    )

    /* function HomeLinks() {
        return (
            <ul className="menu-el sub-menu-items-list">
                <li><button className="btn" >About</button></li>
            </ul>
        )
    }

    function ResumeLinks() {
        return (
            <ul className="menu-el sub-menu-items-list">
                <li><a href={ResumePDF} className="btn">Download PDF</a></li>
            </ul>
        )
    } */

    /* function goToSect(sect) {
        sectHandler(sect)
        document.getElementById('main-content').scrollIntoView({behavior: "smooth"})
    } */
}

export default Navbar