// import Contact from 'components/Contact'
import Details from 'components/Details'
import Logo from 'components/Logo'
import Contact from 'components/Contact'

const Footer = ({mailerState, contactHandler}) => {
    const date = new Date()
    return (
        <footer id="footer" className="section footer-section dark">
            <section id="footer-main-content" className="col-2">
                <div className="col">
                    <Details/>
                </div>
                <div className="col">
                    {/* <Contact mailerState={mailerState} contactHandler={contactHandler}/> */}
                </div>
            </section>
            <section id="footer-sub-content">
                <Logo/>
                <p id="copyright-claim">BJO102 &#169; {date.getFullYear()} All rights reserved.</p>
            </section>
        </footer>
    )
}

export default Footer