const Header = ({children, heading}) => {
    const theChildren = children ? <div className="after-content">{children}</div> : '';
    return (
        <header className="page-header">
            <hgroup id="page-header-hgroup">
                <h1 className="page-heading">{heading}</h1>
                {theChildren}
            </hgroup>
        </header>
    )
}

export default Header