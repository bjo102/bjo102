import Logo from 'components/Logo'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faReact, faPhp, faJsSquare, faWordpressSimple, faDocker } from '@fortawesome/free-brands-svg-icons'

const LandingHeader = () => {
    return (
        <header id="landing-header" className="section dark">
            <figure>
                <Logo/>
                <div className="headings">
                    <h1 className="name-heading heading">Byron Joshua O'Malley</h1>
                    <h2 className="title-heading heading">Web Developer</h2>
                </div>
                <div className="brand-icons-container">
                    <FontAwesomeIcon icon={faWordpressSimple} />
                    <FontAwesomeIcon icon={faPhp} />
                    <FontAwesomeIcon icon={faJsSquare} />
                    <FontAwesomeIcon icon={faReact} />
                    <FontAwesomeIcon icon={faDocker} />
                </div>
                <div className="personal-links-container">
                    <button className="btn contact-btn" onClick={() => document.getElementById('footer').scrollIntoView({ behavior: "smooth" })}>Contact Me</button>
                </div>
            </figure>
        </header>
    )
}

export default LandingHeader