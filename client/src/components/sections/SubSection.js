import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const SubSection = ({ heading, icon, children, className }) => {
    const theIcon = icon ? <FontAwesomeIcon className="fa-icon" icon={icon} /> : ''
    const theHeading = heading ? <h3 className="heading">{heading}</h3> : ''
    const theHeader = (heading || icon) ? <div className="sub-section-header">{theHeading}{theIcon}</div> : ''
    const theClass = className ? ` ${className}` : ''

    return (
        <article className={`sub-section${theClass}`}>
            {theHeader}
            <div className="sub-section-content">
                {children}
            </div>
        </article>
    )
}

export default SubSection