import SubSection from "components/sections/SubSection"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCode, faScrewdriverWrench } from '@fortawesome/free-solid-svg-icons'
import { faWordpress } from '@fortawesome/free-brands-svg-icons'


const Services = () => {
    return (
        <section id="services-section" className="section section-dark">
            <div className="section-content">
                <FontAwesomeIcon className="fa-section-background" icon={faWordpress} />

                <h2 className="heading section-heading">Services</h2>

                <SubSection heading="Web Development" icon={faCode}>
                    <p>
                        Custom solutions for web-based needs,<br/>
                        with a specialisation in <strong>PHP</strong> and <strong>JavaScript</strong>.
                    </p>
                </SubSection>
                
                <SubSection className="reverse" heading="WordPress Theme & Plugin Development" icon={faWordpress}>
                    <p>
                        Specialised PHP templates,<br/>
                        JavaScript UI features and core WordPress utilities.
                    </p>
                </SubSection>

                <SubSection heading="Workflow & Maintenance" icon={faScrewdriverWrench}>
                    <p>
                        Tools for <strong>versioning</strong> and <strong>containerisation</strong> used in<br/>
                        the workflow to ensure that projects are delivered and kept in optimal state.
                    </p>
                </SubSection>
            </div>
        </section>
    )
}

export default Services