import SubSection from "components/sections/SubSection"
import { NavLink } from "react-router-dom";

const About = () => {
    return (
        <section id="about-section" className="section dark">
                <h2>About</h2>

                <SubSection>
                    <p>
                        Byron O'Malley is a front-end web developer with experience in coding with <strong>PHP</strong>, <strong>JavaScript</strong> and <strong>React</strong>.
                    </p>
                    <p>
                        His workflow involves <strong>GitLab</strong> for maintenance, administration and communication with clients and team members, while using <strong>Docker containers</strong> to stage and deploy updates to production.
                    </p>
                    <p>
                        Byron is currently working at <a href="https://dynamicalview.net" target="_blank" rel="noreferrer"><strong>Cloud Daemons Ltd</strong></a> as a remote developer in Ireland, and has been since his internship began in October 2018.
                    </p>

                    <NavLink to="/portfolio" className={({ isActive }) => (isActive ? 'btn active' : 'btn inactive')} tabIndex="0">See Portfolio</NavLink>
                </SubSection>
        </section>
    )
}

export default About