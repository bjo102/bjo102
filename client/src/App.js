import React, { useState, useEffect } from 'react';
// import env from 'dotenv';
import 'css/App.css';
import ProjectSpecs from 'projects/ProjectSpecs'
import Theme from 'components/temp/Theme'

// routes
import Home from 'components/pages/Home'
import Portfolio from 'components/pages/Portfolio'
import FourOFour from 'components/pages/FourOFour'
import FolioPage from 'components/portfolio/FolioPage'
// helpers
// import fetchGraphQL from 'helpers/fetchGraphQL';
import makeCamelCase from 'helpers/makeCamelCase';
import getUrlSlug from 'helpers/getUrlSlug';
import getGitLabProjects from 'helpers/getGitLabProjects';
import getNoGitProjects from 'helpers/getNoGitProjects';
import slugifyStr from 'helpers/slugifyStr';

import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { AppContext } from 'context';
import ScrollToTop from 'ScrollToTop'

/**
 * 
 * @return {JSX}
 */
const App = () => {

    const [Projects, setProjects] = useState(undefined)

    /**
     * The url to graphQL API of the repository.
     * 
     * @since 1.0.0
     * 
     * @var string
     */
    const repoGraphQLURL = 'https://gitlab.com/api/graphql'

    /**
     * The required access token for GitLab API authentication.
     * 
     * @since 1.0.0
     * 
     * @var string
     */
    const repoToken = process.env.REACT_APP_GITLAB_AUTH_TOKEN

    /**
     * The check for the mounted status of the app.
     * 
     * @since 1.0.0
     * 
     * @var bool
     */
    let isMounted = true

    /**
     * Get list items from markdown by parsing the content between two consecutive headings.
     * 
     * The first heading must be at level-two (##). Once the text of the first heading is set,
     * all content between this and the next heading, of any level, will be parsed for list items.
     * 
     * Then a second parsing is taken for this section of the markdown, where patterns resembling list items
     * are used to create items for the list array.
     * 
     * @since 1.0.0
     * 
     * @todo This does not support special characters or punctuation. 
     * @param {string} md The markdown to parse.
     * @param {string} heading The first heading to look for in the markdown.
     * @return {array} The list items.
     */
    const getMarkdownList = (md, heading) => {
        const listSectReg = new RegExp(`(## ${heading})([a-z\\s\\-\\_]+)([^#+])`, 'gim')
        const listItemReg = /-(.*).?\n/gim

        if (listSectReg.test(md)) {
            let listSect = md.match(listSectReg)[0]
            let listItems = listSect.match(listItemReg)
            let array = listItems.map(li => li.replace(/^(- )|(\n)$/g, '').trim())
            return array
        } else {
            return []
        }
    }

    /**
     * Prepares the GraphQL query by loading project ids and defining the schema for the GitLab GraphQL API.
     * 
     * @since 1.0.0
     * 
     * @todo The repository query for files accepts only "main" as the branch, this is a problem for "master" or other synonyms.
     * @return {string} GraphQL query.
     */
    const getGraphQLQuery = ids => {
        const locations = ids.map(id => `"gid://gitlab/Project/${Number(id)}"`)
        const graphQLQuery = `{
            projects(membership: true, ids:[${locations.join(', ')}]) {
                nodes {
                    id
                    name
                    description
                    webUrl
                    httpUrlToRepo
                    repository {
                        blobs(ref:"main", paths: ["README.md"]) {
                            nodes {
                                rawTextBlob
                            }
                        }
                    }
                }
                pageInfo {
                    endCursor
                    hasNextPage
                }
            }
        }`

        return graphQLQuery
    }

    /**
     * Fetch GraphQL data.
     * 
     * @since 1.0.0
     * 
     * @param {string} url The location for the fetch request.
     * @param {string} query Query variabales.
     * @param {string} variables 
     * @param {string} token The Bearer access token for header.
     * @returns JSON response data.
     */
    const fetchGraphQL = async (url, query, variables, token) => {  
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: "application/json",
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify({
                query: query,
                variables,
            }),
        })

        return await response.json()
    }

    /**
     * 
     * Avoid updating state if the component unmounted before the fetch completes.
     * 
     * @return project data
     */
    const defineRemoteData = (response, isMounted) =>
    {
        if (! isMounted) {
            return;
        } else {
            let projects = response.data.projects.nodes
            return projects
        }
    }

    /**
     * A Project should have the following scheme:
     * 
     * @param {object} projs
     * @param {array} data
     */
    const setGitLabProjects = (projs, data) => {

        let bindedProjs = {}

        data.forEach(d => {
            let projSlug = getUrlSlug(d.webUrl)
            let projKey = makeCamelCase(projSlug, /[-_]/)

            if (projs[projKey]) {
                let obj = projs[projKey]
                let readme = d.repository.blobs.nodes[0]
                
                obj.slug = d.webUrl ? getUrlSlug(d.webUrl) : ''
                obj.source = 'gitlab'
                obj.title = d.name ? d.name : ''
                obj.link = d.webUrl ? d.webUrl : ''
                obj.repoLink = d.httpUrlToRepo ? d.httpUrlToRepo : ''
                obj.description = d.description ? d.description : ''

                if (readme) {
                    let readmeRaw = readme.rawTextBlob ? readme.rawTextBlob : '' 
                    obj.readmeRaw = readmeRaw
                    obj.features = getMarkdownList(readmeRaw, 'features')
                    obj.technologies = getMarkdownList(readmeRaw, 'technologies')
                }

                bindedProjs[projKey] = obj
            }
        })
        return bindedProjs
    }

    /**
     * Prepare data for No-Git projects.
     * 
     * @param {object} projs 
     * @returns The projects
     */
    const setNoGitProjects = (projs) => {
        Object.keys(projs).forEach(key => {
            // projs[key].slug = slugifyStr(key.split(/(?<=[a-z])(?=[A-Z])/).join('-'))
            projs[key].slug = slugifyStr(key.split(/([a-z]+|[A-Z][a-z]*)/g).join('-'))
        })

        return projs
    }

    /**
     * 
     * @since 1.0.0
     * 
     * @returns All projects, those with and without repository data.
     */
    const setTheProjects = async () => {
        const gitProjs = getGitLabProjects(ProjectSpecs)
        const gitProjIds = Object.values(gitProjs).map(val => val.id)
        const graphQLQuery = getGraphQLQuery(gitProjIds)
        const bindedGitProjs = await fetchGraphQL(repoGraphQLURL, graphQLQuery, null, repoToken)
            .then(response => defineRemoteData(response, isMounted))
            .then(gitData => setGitLabProjects(gitProjs, gitData))
            .catch(error => { throw new Error(`There was an issue: ${error.message}`) })
        
        const noGitProjs = getNoGitProjects(ProjectSpecs)
        const bindedNoGitProjs = await setNoGitProjects(noGitProjs)

        const allProjects = {...bindedGitProjs, ...bindedNoGitProjs}

        return allProjects
    }

    useEffect(() => {
        setTheProjects()
            .then(projs => {
                setProjects(projs)
            })
        return () => { isMounted = false }
    }, [repoToken])

    return (
        <AppContext.Provider value={{ Projects }}>
            <BrowserRouter>
                <ScrollToTop>
                    <Routes>
                        <Route path="/"             element={<Theme><Home/></Theme>} />
                        <Route path="/portfolio"    element={<Theme><Portfolio/></Theme>} />
                        <Route path="/portfolio/*"  element={<Theme><FolioPage/></Theme>} />
                        <Route path="*"             element={<Theme><FourOFour/></Theme>} />
                    </Routes>
                </ScrollToTop>
            </BrowserRouter>
        </AppContext.Provider>
    )
}

export default App