import fetch from 'node-fetch';
import express from 'express';
import nodemailer from 'nodemailer';
import cors from 'cors';
//import env from 'dotenv';
import bodyParser from 'body-parser'
import { check, validationResult } from 'express-validator'
import fs from 'fs'

/**
 * The parsed values of the .env file.
 * 
 * @since 1.0.0
 * 
 * @var object
 */
const Env = process.env

/**
 * The server app.
 * 
 * @since 1.0.0
 */
const app = express();

/**
 * Transporter specifications for email services.
 * 
 * @since 1.0.0
 * 
 * @var object
 */
const transporterObj = {
    service: "gmail",
    auth: {
      type: "OAuth2",
      user: Env.EMAIL,
      pass: Env.EMAIL_PASS,
      clientId: Env.EMAIL_OAUTH_CLIENTID,
      clientSecret: Env.EMAIL_OAUTH_CLIENT_SECRET,
      refreshToken: Env.EMAIL_OAUTH_REFRESH_TOKEN,
    },
}

/**
 * The port for the server to run on.
 * 
 * @since 1.0.0
 * 
 * @var int
 */
 const port = Env.SERVER_APP_PORT;

/**
 * The NodeMailer transporter for email services
 * 
 * @since 1.0.0
 */
const transporter = nodemailer.createTransport(transporterObj);

/**
 * Log the transporter verification results.
 * 
 * @since 1.0.0
 * 
 * @param {object} err 
 * @param {boolean} success 
 * @return The error/success log.
 */
const transScs = (err, success) => err ? console.log(err) : console.log(`=== Server is ready to take messages: ${success} ===`);

/**
 * The process for sending emails via contact form.
 * 
 * @since 1.0.0
 * 
 * @param {object} req The request sent from the client.
 * @param {object} res The response to be sent from the server.
 */
const sendMail = async (req, res) => {
    const errors = validationResult(req);
    const mailerState = req.body.mailerState;
    const recaptchaVal = req.body.mailerState.recaptchaValue;
    const recaptchaSecret = process.env.RECAPTCHA_SECRET_KEY
    const rcptVerifyURL = `https://www.google.com/recaptcha/api/siteverify?secret=${recaptchaSecret}&response=${recaptchaVal}`
    let rcptStatus = null
    const mailOptions = {
        from: `${req.body.mailerState.email}`,
        to: Env.EMAIL,
        subject: `Message from: ${req.body.mailerState.email}`,
        text: `${mailerState.message}`,
    };

    await fetch(rcptVerifyURL, { method: "post" })
        .then(response => response.json())
        .then(google_response => rcptStatus = google_response.success)
  
    if (!rcptStatus) {
        return res.status(400).json({ status: "fail", errors: ["You are not human!"] });
    }

    if (!errors.isEmpty()) {
        return res.status(400).json({ status: "fail", errors: errors.array() });
    }

    transporter.sendMail(mailOptions, (err, data) => {
        if (err) {
            res.json({
                status: "fail",
            });
        } else {
            console.log("== Message Sent ==");
            res.json({
                status: "success",
            });
        }
    });
}

/**
 * Retrieve data for a repository via APIs.
 * 
 * @since 1.0.0
 * 
 * @param {object} req The request sent from the client.
 * @param {object} res The response to be sent from the server.
 */
const getFolioItemData = (req, res) => {
    const projId = req.query.id;
    const wikiPg = req.query.page ? req.query.page : 'devLog';
    // const wikiApi = `https://gitlab.com/api/v4/projects/${projId}/wikis/${wikiPg}?render_html=true`
    const wikiApi = `https://gitlab.com/api/v4/projects/${projId}/wikis/${wikiPg}`
    const langApi = `https://gitlab.com/api/v4/projects/${projId}/languages`
    const data = {}

    Promise.all([
        fetch(wikiApi).then(fetchRes => fetchRes.json()).then(fJson => data.wiki = fJson),
        fetch(langApi).then(fetchRes => fetchRes.json()).then(fJson => data.langs = fJson),
    ]).then(() => {
        res.send(data)
    })
}

const getFolioItemDevLog = (req, res) => {
    const devLog = req.query.project
    const url = `/app/client/devLog/${devLog}.md`
    
    try {
        const data = fs.readFileSync(url, 'utf8');
        res.send(data)
    } catch (err) {
        console.error(err);
    }
}

app.use(bodyParser.json())
app.use(express.json());
app.use(cors());
// transporter.verify(transScs);

app.post(
    "/send",
    check('mailerState.name').isLength({ min: 1, max: 100}),
    check('mailerState.company').optional({checkFalsy: true}).isLength({ min: 1, max: 100 }),
    check('mailerState.email').isEmail(),
    check('mailerState.message').isLength({ min: 1, max: 400}),
    sendMail
);

app.get(
    "/folio-item",
    getFolioItemData
);

app.get(
    "/devLog",
    getFolioItemDevLog
);

app.listen(
    port,
    () => console.log(`Server is running on port: ${port}`)
);